# Megaport Tech Assessment - Activity 1

Project for Activity 1 on Megaport's frontend dev tech assessment.

## Description

React project that takes baked goods data and displays it in a table that is sortable and filterable. Also includes a form to add more baked goods.

## Assumptions &amp; Comments:

This task was given in a vacuum without real-world context, with an expectation of an effort of not much more than 2 hours. Therefore, there were assumptions I made as well as some UX decisions I made in a generic way. Given an actual scenario, the technical and visual design may very well have been different.

1. **Placement of filter and "Add New" button** - as a generic design decision, I've placed these in a very visible, prominent place so that it won't be missed by the user. But if most users were mobile users, likely any action buttons would go in the bottom right of the screen in a fixed location, easily accessible by a thumb or right index finger.

2. **Mobile-first / mobile friendly** - the screen has a limited amount of responsiveness, but given the LOE assumption, I did not make it fully mobile friendly.

3. **Using a modal for the form input** - modals can be a nice way to separate out an Add New form and keep the main screen leaner and cleaner. However, depending on the way users use the form, there could be instances where an inline form that is readily available (without having to click a button to open a modal) might be better.

4. **DataTable component taking predefined Item[] type** - I'm defining an Item[] type based on the data given, in light of the expected LOE. Ideally, it would be nice to use generics so that the DataTable component could accept any table-like data shape.

5. **`id` field is not unique** - I noticed the `id` field is not a unique/primary key, but rather corresponds to `type`/`name` pairs. Because of this and (once again) the LOE assumption, I did not automatically generate a new id when adding a new baked good, but just left it as a free-form field.

## Prerequisites

Before you begin, ensure you have the following installed:

- [Node.js](https://nodejs.org/en/download/) (includes npm)

## Install

Follow these steps to install the project locally:

1. **Clone the repository:**

   ```sh
   git clone https://gitlab.com/joelbyrd/megaport-tech-assessment-activity-1.git
   ```

2. **Navigate to the project directory:**

   ```sh
   cd your-repository-name
   ```

3. **Install dependencies:**

   ```sh
   npm install
   ```

## Run

Once the dependencies are installed, you can run the project locally using the following command:

```sh
npm start
```

This will start the development server and you can then view the project in your browser at `http://localhost:3000`.

## Build

If you want to create a production-ready build, you can run the following command:

```sh
npm run build
```

This creates an optimized production build of the application in a `build` directory. If you want to run this production build in the browser to test it locally, you can use a simple HTTP server package like `serve`. To do so, run the following:

```sh
npm install -g serve
serve -s build
```

You can then open your browser and navigate to the address provided by the server (usually `http://localhost:5000` or a similar URL) to see your production build.
