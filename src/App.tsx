// types
import { Item } from "./hooks/useTableData.d";

// components
import { DataTable } from "./components";

// data
import data from "./data/bakedGoods.json";

// styles
import "./styles/reset.css";
import "./styles/base.css";
import "./styles/form.css";
import "./App.css";
import "./styles/util.css";

const bakedGoods: Item[] = data as Item[];

function App() {
  return (
    <div className="app">
      <div className="app-content">
        <h1>Baked Goods</h1>
        <DataTable
          data={bakedGoods}
          showFilter
          showAddNewItem
          sortableColumns={["id", "type", "topping"]}
        />
      </div>
    </div>
  );
}

export default App;
