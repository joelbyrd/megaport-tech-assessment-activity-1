import React, { useState } from "react";

// hooks
import useTableData from "../../hooks/useTableData";

// types
import { Item } from "../../hooks/useTableData.d";

// components
import { Modal } from "../Modal";
import { AddItemForm } from "../AddItemForm";

// styles
import "./DataTable.css";

interface Props {
  data: Item[];
  showFilter?: boolean;
  showAddNewItem?: boolean;
  sortableColumns?: (keyof Item)[];
}

const DataTable: React.FC<Props> = ({
  data,
  showFilter = false,
  showAddNewItem = false,
  sortableColumns = [],
}) => {
  const {
    data: tableData,
    handleSort,
    handleFilterChange,
    addItem,
    sortCriteria,
  } = useTableData(data);
  const [isModalOpen, setIsModalOpen] = useState(false);

  const getSortClass = (key: keyof Item) => {
    if (sortCriteria && sortCriteria.key === key) {
      return sortCriteria.direction === "asc" ? "sort-asc" : "sort-desc";
    }
    return "";
  };

  return (
    <div className="mpt-datatable-container">
      {(showFilter || showAddNewItem) && (
        <div className="flex col-gap-10">
          {showFilter && (
            <input
              type="text"
              placeholder="Search..."
              onChange={(e) => handleFilterChange(e.target.value)}
            />
          )}
          {showAddNewItem && (
            <button onClick={() => setIsModalOpen(true)}>+ Add New</button>
          )}
        </div>
      )}
      <div className="table-container">
        {tableData.length > 0 ? (
          <table className="table">
            <thead>
              <tr>
                {(Object.keys(tableData[0]) as (keyof Item)[]).map((key) => (
                  <th
                    key={key}
                    className={
                      sortableColumns.includes(key)
                        ? `capitalize sortable ${getSortClass(key)}`
                        : "capitalize"
                    }
                    onClick={() =>
                      sortableColumns.includes(key) && handleSort(key)
                    }
                  >
                    {key}
                  </th>
                ))}
              </tr>
            </thead>
            <tbody>
              {tableData.map((item, index) => (
                <tr key={index}>
                  {Object.keys(item).map((key) => (
                    <td key={key}>{item[key as keyof Item]}</td>
                  ))}
                </tr>
              ))}
            </tbody>
          </table>
        ) : (
          <div className="alert alert-warning" role="alert">
            No results. You may need to clear your filter or filter by another
            value.
          </div>
        )}
        {showAddNewItem && (
          <Modal
            title="Add Baked Good"
            isOpen={isModalOpen}
            onClose={() => setIsModalOpen(false)}
          >
            <AddItemForm
              dataItem={tableData[0]}
              onAdd={(item) => {
                addItem(item);
                setIsModalOpen(false);
              }}
              onClose={() => setIsModalOpen(false)}
            />
          </Modal>
        )}
      </div>
    </div>
  );
};

export default DataTable;
