import React, { useState, useEffect, useRef, FormEvent } from "react";

// types
import { Item } from "../../hooks/useTableData.d";

interface AddItemFormProps {
  onAdd: (item: Item) => void;
  onClose: () => void;
  dataItem: Item; // used only to get the keys of the object
}

const AddItemForm: React.FC<AddItemFormProps> = ({
  onAdd,
  onClose,
  dataItem,
}) => {
  const [formData, setFormData] = useState<Required<Item>>(
    {} as Required<Item>
  );

  // Focus the first input field when the form is rendered
  const [firstFieldFocused, setFirstFieldFocused] = useState(false);
  const firstInputRef = useRef<HTMLInputElement>(null);
  useEffect(() => {
    if (firstInputRef.current && !firstFieldFocused) {
      firstInputRef.current.focus();
      setFirstFieldFocused(true);
    }
  }, [firstFieldFocused]);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setFormData((prevFormData) => ({
      ...prevFormData,
      [name]: value,
    }));
  };

  const handleSubmit = (e: FormEvent) => {
    e.preventDefault();
    onAdd(formData as Item);
    onClose();
  };

  return (
    <form onSubmit={handleSubmit}>
      {/* Loop through the keys of the dataItem object to create the form fields */}
      {Object.keys(dataItem).map((key, index) => (
        <div className="form-group" key={key}>
          <label htmlFor={key} className="capitalize">
            {key}
          </label>
          <input
            aria-label={key}
            className="form-control"
            type="text"
            id={key}
            name={key}
            value={formData[key as keyof Item]}
            onChange={handleChange}
            required
            // need ref to focus the first input field
            ref={index === 0 ? firstInputRef : null}
          />
        </div>
      ))}
      <div className="flex justify-end col-gap-10 mt-4">
        <button type="button" onClick={onClose} className="secondary-btn">
          Cancel
        </button>
        <button type="submit">Add Baked Good</button>
      </div>
    </form>
  );
};

export default AddItemForm;
