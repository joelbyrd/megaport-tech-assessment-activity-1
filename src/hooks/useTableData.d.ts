export interface Item {
  id: string;
  type: string;
  name: string;
  topping: string;
}

export interface SortCriteria {
  key: keyof Item;
  direction: "asc" | "desc";
}
