import { useState, useEffect } from "react";

// types
import { Item, SortCriteria } from "./useTableData.d";

const useTableData = (initialData: Item[]) => {
  const [data, setData] = useState(initialData);
  const [sortCriteria, setSortCriteria] = useState<SortCriteria | null>(null);
  const [filterText, setFilterText] = useState<string>("");
  const [filteredData, setFilteredData] = useState<Item[]>(initialData);

  const handleSort = (key: keyof Item) => {
    setSortCriteria((prevSortCriteria) =>
      prevSortCriteria && prevSortCriteria.key === key
        ? {
            key,
            direction: prevSortCriteria.direction === "asc" ? "desc" : "asc",
          }
        : { key, direction: "asc" }
    );
  };

  const handleFilterChange = (text: string) => {
    setFilterText(text);
  };

  const addItem = (item: Item) => {
    setData((prevData) => [...prevData, item]);
  };

  useEffect(() => {
    const sortData = (dataToSort: Item[]): Item[] => {
      if (sortCriteria) {
        return [...dataToSort].sort((a, b) => {
          const valueA = a[sortCriteria.key];
          const valueB = b[sortCriteria.key];

          if (typeof valueA === "string" && typeof valueB === "string") {
            const comparison = valueA.localeCompare(valueB, undefined, {
              sensitivity: "base",
            });
            return sortCriteria.direction === "asc" ? comparison : -comparison;
          }

          if (valueA < valueB) return sortCriteria.direction === "asc" ? -1 : 1;
          if (valueA > valueB) return sortCriteria.direction === "asc" ? 1 : -1;
          return 0;
        });
      }
      return dataToSort;
    };

    const filterData = (dataToFilter: Item[]): Item[] => {
      if (filterText) {
        return dataToFilter.filter((item) =>
          Object.values(item).some((value) =>
            value.toString().toLowerCase().includes(filterText.toLowerCase())
          )
        );
      }
      return dataToFilter;
    };

    let updatedData = filterData(data);
    updatedData = sortData(updatedData);
    setFilteredData(updatedData);
  }, [data, sortCriteria, filterText]);

  return {
    data: filteredData,
    handleSort,
    handleFilterChange,
    addItem,
    sortCriteria,
  };
};

export default useTableData;
